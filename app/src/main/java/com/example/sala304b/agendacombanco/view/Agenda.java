package com.example.sala304b.agendacombanco.view;

import android.graphics.Bitmap;

import java.io.Serializable;



public class Agenda implements Serializable {


    private String id ;
    private String nome ;
    private String endereço ;
    private String telefone ;
    private String celular ;
    private  transient Bitmap foto  ;


    public Agenda() {
    }


    public Agenda(String nome, String endereço, String telefone, String celular, Bitmap foto) {
        this.nome = nome;
        this.id = id;
        this.endereço = endereço;
        this.telefone = telefone;
        this.celular = celular;
        this.foto = foto;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEndereço() {
        return endereço;
    }

    public void setEndereço(String endereço) {
        this.endereço = endereço;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public String getCelular() {
        return celular;
    }

    public void setCelular(String celular) {
        this.celular = celular;
    }

    public Bitmap getFoto() {
        return foto;
    }

    public void setFoto(Bitmap foto) {
        this.foto = foto;
    }
}
