package com.example.sala304b.agendacombanco.view;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

public class AgendaBanco extends SQLiteOpenHelper {

    private static final String DATABASE = "SQLite" ;
    private static final int VERSAO = 1 ;

    public AgendaBanco(Context context) {
        super(context, DATABASE , null, VERSAO);
    }


    public AgendaBanco(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase ) {
        String age = "CREATE TABLE Agenda " +
                "( id PRIMARY KEY ," +
                "nome TEXT NOT NULL , " +
                "enderoco TEXT , " +
                "telefone TEXT , " +
                "celular TEXT , " ;

        sqLiteDatabase.execSQL(age);

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

        String age = "DROP TABLE IF EXISTS Agenda ;" ;
        sqLiteDatabase.execSQL(age);
        this.onCreate(sqLiteDatabase);

    }

    public void salvar(Agenda agenda) {

        ContentValues values = new ContentValues();
        values.put("nome" ,agenda.getNome());
        values.put("nome" ,agenda.getEndereço());
        values.put("nome" ,agenda.getTelefone());
        values.put("nome" ,agenda.getCelular());

        getWritableDatabase().insert(
                "Agenda" ,
                null,
                values
        );
    }

    /*public List<Agenda> getLista() {
        List<Agenda> lista = new ArrayList<>();
        String colunas[] = {"id" , "nome" , "telefone" , "email" , "site"} ;

        Cursor cursor = getWritableDatabase().query(
                "Agenda" ,
                colunas,
                null,
                null,
                null,
                null,
                null);

        while(cursor.moveToNext()) {

             Agenda agenda = new Agenda();
             agenda.setId(cursor.getInt(0);
             agenda.setNome(cursor.getString(1));


        }







    }*/




}
