package com.example.sala304b.agendacombanco.view;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.example.sala304b.agendacombanco.R;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    private ListView listagenda ;
    private List<Agenda> lista ;
    private ArrayAdapter<Agenda> adapter ;
    private AgendaBanco agendaBanco ;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

    }

    @Override
    protected void onResume() {
        super.onResume();

        /*aqui criar meu banco de dados depois*/
        agendaBanco = new AgendaBanco(this) ;
        lista = agendaBanco.getLista();
        agendaBanco.close();

        adapter = new ArrayAdapter<Agenda>(this , android.R.layout.simple_list_item_1, lista);

        listagenda.setAdapter(adapter);


    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu , menu);

        return true ;

    }

    public void Novo(MenuItem item) {

        Intent intent = new Intent(this, Novo_Activity.class);
        startActivity(intent);
    }

    public void Sobre(MenuItem item) {

        Intent intent = new Intent(this, Sobre_Activity.class);
        startActivity(intent);
    }





}
